Provides a 'News Zipper' or running display.

Authors:
  Luch Klooster

Sponsors:
  CONVENT web design and development


Requirements
------------

1. none


Installation
------------

1. Place this module directory in your modules folder (usually sites/all/modules/).

2. Go to "Administer" > "Site Building" > "Modules" and enable the module.

3. Under /admin/structure/block you now find a clock named Jqscroller.


Configuration
-------------

1. Go to "Administer" > "Configuration" > "Media" </admin/config/media/jqscroller> to edit the settings.

2. You can set the scroll speed and refreshrate. Play with these setting to get a smooth scrolling.

3. There are a few display settings: Scroll direction: Left (default), Right, Up and Down. Under Node Type you have to give the node type that is to be used to fetch the text to display. You can limit the number of titles that are displayed.

Best used is a node type with just a title and a plain text body field.

Font
----

Using font LEDBOARDREVERSED is giving a nice effect. Download the font from http://www.fonts2u.com/led-board-reversed.font
Convert the font to the desired formats on http://www.font2web.com/
The file jqscroller.css already contains the lines to use @font-face. All you have to do is uncomment the lines.